# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/repos/aqua_computer/tests/libs/googletest/googletest/src/gtest-all.cc" "D:/repos/aqua_computer/bin/tests/libs/googletest/googlemock/CMakeFiles/gmock.dir/__/googletest/src/gtest-all.cc.obj"
  "D:/repos/aqua_computer/tests/libs/googletest/googlemock/src/gmock-all.cc" "D:/repos/aqua_computer/bin/tests/libs/googletest/googlemock/CMakeFiles/gmock.dir/src/gmock-all.cc.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src"
  "../tests/libs/googletest/googlemock/include"
  "../tests/libs/googletest/googlemock"
  "../tests/libs/googletest/googletest/include"
  "../tests/libs/googletest/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
