# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/repos/aqua_computer/src/app.cpp" "D:/repos/aqua_computer/bin/tests/UT/CMakeFiles/AquaLamp_UTs.dir/__/__/src/app.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src"
  "../tests/libs/googletest/googletest/include"
  "../tests/libs/googletest/googletest"
  "../tests/libs/googletest/googlemock/include"
  "../tests/libs/googletest/googlemock"
  "../test/libs/CppFreeMock"
  "../test/UT_AVR/hw_stub"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "D:/repos/aqua_computer/bin/tests/libs/googletest/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "D:/repos/aqua_computer/bin/tests/libs/googletest/googlemock/CMakeFiles/gmock.dir/DependInfo.cmake"
  "D:/repos/aqua_computer/bin/tests/libs/googletest/googlemock/gtest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
